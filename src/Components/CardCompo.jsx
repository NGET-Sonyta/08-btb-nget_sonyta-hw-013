import React, { Component } from 'react'
import { Button, Card, Container, Row, Col } from 'react-bootstrap';
import Axios from 'axios';
import {BrowserRouter as Router,Link,Route} from 'react-router-dom';

export default class CardCompo extends Component {
    constructor(){
        super()
        this.state={
            data:{
                PERSON:[]
            },
            TITLE: '',
            imagePreviewUrl: '',
            file: '',
        }
    }
    _handleImageChange(e) {
        e.preventDefault();
        
        let reader = new FileReader();
        let file = e.target.files[0];
    
        reader.onloadend = () => {
          this.setState({
            file: file,
            imagePreviewUrl: reader.result
          });
        }
    
        reader.readAsDataURL(file)
      }
    
    handleChange = event =>{
        this.setState({TITLE: event.target.value});
    }
    preview_image(event) 
    {
     var reader = new FileReader();
    reader.onload = function()
     {
      var output = document.getElementById('output_image');
      output.src = reader.result;
     }
     console.log(event.target.files[0]);
     
     reader.readAsDataURL(event.target.files[0]);
    }
    componentWillMount(){
        Axios
        .get(`http://110.74.194.124:15011/v1/api/articles/${this.props.match.params.id}`)
        .then(res=>{
            this.setState({
                data: res.data,
                TITLE: res.data.DATA.TITLE,
                DESCRIPTION: res.data.DATA.DESCRIPTION,
                IMAGE: res.data.DATA.IMAGE
            })
            console.log(res.data.DATA);
            console.log(res.data.DATA.TITLE);
        } )
    }
    render() {        
        return (
            <Container>
                <br/>
                <br/>
                <Card style={{textAlign:"center", backgroundColor:"#e0f2f1"}}>
                <Card.Img variant="top" src={this.state.IMAGE} />
                <Card.Body>
                <h1 style={{backgroundColor:"#80cbc4"}}>{this.state.TITLE}</h1>
                <hr/>
                <br/>
                    <Card.Text>
                    {this.state.DESCRIPTION}
                    </Card.Text>
                    <Button variant="danger" as={Link} to="/Home">Close</Button>
                </Card.Body>
                </Card>
            </Container>
            
        )
    }
}
