import React, { Component } from 'react'
import {Navbar, Button, Form, FormControl, Nav} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter as Router,Link,Route} from 'react-router-dom';

export default class NavBarr extends Component {
    render() {
        return (
            <div>
                <Navbar style={{backgroundColor:"#26a69a"}}>
                    <Navbar.Brand as={Link} to="/Home" style={{color:"white"}}>AMS</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                            <Nav.Link as={Link} to="/Home" style={{color:"white"}}>Home</Nav.Link>
                        </Nav>
                        <Form inline>
                        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                        <Button style={{color:"white", borderColor:"white", backgroundColor:"#26a69a"}}>Search</Button>
                        </Form>
                    </Navbar.Collapse>
                </Navbar>
            </div>
        )
    }
}
