import React, { Component } from 'react';
import Axios from 'axios';
import { Button , Row, Container} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

export default class UpdateCompo extends Component {
   
    constructor(){
        super()
        this.state={
            data:{
                PERSON:[]
            },
            TITLE: '',
            imagePreviewUrl: '',
            file: '',
            DESCRIPTION: '',
        }
        this.handleUpdate=this.handleUpdate.bind(this);
        this.handleChange=this.handleChange.bind(this);
    }
    async componentWillMount() {
        const user= {
            TITLE: this.state.TITLE
        }
        Axios
        .get(`http://110.74.194.124:15011/v1/api/articles/${this.props.match.params.id}`)
        .then(res=>{
            this.setState({
                data: res.data,
                TITLE: res.data.DATA.TITLE,
                DESCRIPTION: res.data.DATA.DESCRIPTION,
                imagePreviewUrl: res.data.DATA.IMAGE
            })
            console.log(res.data.DATA.TITLE);
        } )
      }
    _handleImageChange(e) {
        e.preventDefault();
        
        let reader = new FileReader();
        let file = e.target.files[0];
    
        reader.onloadend = () => {
          this.setState({
            file: file,
            imagePreviewUrl: reader.result
          });
        }
    
        reader.readAsDataURL(file)
      }
    
    handleTitle = event =>{
        this.setState({TITLE: event.target.value});
    }
    handleDescription = event =>{
        this.setState({DESCRIPTION: event.target.value});
    }
    preview_image(event) 
    {
     var reader = new FileReader();
    reader.onload = function()
     {
      var output = document.getElementById('output_image');
      output.src = reader.result;
     }
     console.log(event.target.files[0]);
     
     reader.readAsDataURL(event.target.files[0]);
    }

    handleUpdate(){
        var titlee= document.getElementById("title").value;
        var desc=document.getElementById("des").value;
        
        Axios.put(`http://110.74.194.124:15011/v1/api/articles/${this.props.match.params.id}`, 
        {
          "TITLE": titlee,
          "DESCRIPTION": desc,
          "IMAGE": this.state.imagePreviewUrl
        }
        ).then(res=>{
        alert("Yayyyy! Successfully Updated.")
        console.log(res.data);
        
            }
        ).catch(error => { 
            alert("error");
            console.log("Error");
                })
            }
    handleChange(e) {
        
        console.log("This"+e.target.value);
      }
      addDefaultSrc(ev){
        ev.target.src = 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTGgs4VcQSVuTo36Y8phFXb28M7KPnkvbGdQ6EZSOUGzil8jH0P&usqp=CAU'
      }

    render() {
        let styleA={background:"#e0f2f1"}
        let styleB={textAlign:"center"}
        let styleC={display:"flex"}
        let styleD={color:"red", display:"none"}
        let styleE={width:"600px"}
        let styleF={color:"red", display:"none"}
        let styleG={marginRight:"50px"}
        let styleH={marginLeft:"50px"}
        let styleI={marginTop:"30px"}
        let styleJ={marginTop:"30px",marginLeft: "250px",width: "150px"}
        console.log(this.props.match.params.id);

        return (
  
            <div style={styleA}>
            <h3 style={styleB}>Update an Article</h3>
            <Container style={{styleC}}>
                <Row style={styleG}>
                  <col-9>
                    <div >
                      <div>
                        TITLE <span id="text1" style={styleD}> Sorry! The TITLE cannot be the same.</span>
                      </div>
                      <input id="title" style={styleE} type="text" value={this.state.TITLE} onChange={this.handleTitle} />
                      <div style={styleI}>Description <span id="text2" style={styleF}> Sorry! The DESCRIPTION cannot be the same</span></div>
                      <input type="text" id="des" style={styleE} value={this.state.DESCRIPTION} onChange={this.handleDescription}/>
                    </div>
                    <Button variant="info" style={styleJ} onClick={this.handleUpdate} type="submit">
                            Update
                    </Button>
                  </col-9>
                  <col-9 style={styleH}>
                    <div >
                    <img width="155px" id="output_image" onError={this.addDefaultSrc} 
                      src={this.state.imagePreviewUrl} />
                          <input className="fileInput" type="file"  onChange={(e)=>this._handleImageChange(e)} />
                    </div>
                  </col-9>
                </Row>
            </Container>
            </div>
        )
    }
}
