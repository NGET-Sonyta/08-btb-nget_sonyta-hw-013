import React, { Component } from 'react';
import Axios from 'axios';
import ArticleInput from './ArticleInput'
import { restElement } from '@babel/types';
import { Table, Button, Card, Container } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter as Router,Link,Route} from 'react-router-dom';
import axios from 'axios';

export default class List extends Component {
    
    state = {
        persons: []
      }
     async componentWillMount() {
        Axios.get(`http://110.74.194.124:15011/v1/api/articles?page9&limit=15`)
          .then(res => {
            const persons = res.data.DATA;
            this.setState({ persons });  
          })     
      }
      convertDate=(dateStr)=>{
        var dateString = dateStr;
        var year = dateString.substring(0, 4);
        var month = dateString.substring(4, 6);
        var day = dateString.substring(6, 8);
        var date = year + "-" + month + "-" + day;
        return date;
    }

    handleDelete(id){
        axios.delete(`http://110.74.194.124:15011/v1/api/articles/${id}`)
        .then(res => console.log(res.data));
        alert("Yayyy! Successfully Delete.");
        this.componentWillMount();
    }
    render() {

        let users=this.state.persons.map((user, index)=>  
        <tr key={index}> 
                        <td >{user.ID}</td>
                        <td >{user.TITLE}</td>
                        <td > {user.DESCRIPTION}</td>
                        <td >{this.convertDate(user.CREATED_DATE)}</td>
                        <td ><img src={user.IMAGE} style={{width:"300px"}}/></td>
                        <td>
                            <div >
                                    <Button variant="success" as={Link} to={`/View/id=${user.ID}`}>View</Button>{' '}
                                    <Button variant="warning" as={Link} to={`/Update/id=${user.ID}`}>Edit</Button>{' '}
                                    <Button variant="danger" onClick={() => this.handleDelete(user.ID)}>Delete</Button> 
                            </div>
                        </td>
                    </tr>             
        )

        let styleA={backgroundColor:"#e0f2f1"}
        let styleB={textAlign:"center", color:"#004d40"}
        let styleC={marginLeft:"45%", marginTop:"15px", marginBottom:"15px", backgroundColor:"#26a69a"}
        let styleD={marginBottom:"0px"}
        let styleE={textAlign:"center", backgroundColor:"#26a69a", color:"white"}
        let styleF={width:"230px"}
        let styleG={backgroundColor:"#26a69a", height:"50px"}
        return (
            <div>
            <Container style={styleA}>
                <br/>
                <h1 style={styleB}>Article Management</h1>
                <Button as={Link} to="/ArticleInput" style={styleC}>Add New Article</Button>
                <br/>
                <br/>
                <Table striped bordered hover style={styleD}>
                    <thead>
                        <tr style={styleE}>
                        <th>#</th>
                        <th>TITLE</th>
                        <th>DESCRIPTION</th>
                        <th>CREATED DATE</th>
                        <th>IMAGE</th>
                        <th style={styleF}>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        {users}
                    </tbody>
                </Table>
            </Container>
            <div style={styleG}></div>
            </div>
        )
    }
}
