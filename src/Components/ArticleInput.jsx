import React, { Component } from 'react';
import Axios from 'axios';
import { Button , Form, Container, Row} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { MDBContainer, MDBAlert } from 'mdbreact';

export default class ArticleInput extends Component {
    constructor(){
        super()
        this.state={
            TITLE: '',imagePreviewUrl: '', file: '',
        }
        this.handleAdd=this.handleAdd.bind(this);
    }
    _handleImageChange(e) {
        e.preventDefault();
        Axios.post('http://110.74.194.124:15011/v1/api/uploadfile/single', 
        {
            
                "CODE": "string",
                "MESSAGE": "string",
                "DATA": this.state.imagePreviewUrl
        } ).then(res=>{
            console.log(res);
            this.setState({
                imagePreviewUrl: res
              });
            console.log(this.state.imagePreviewUrl);
              }
        );
        let reader = new FileReader();
        let file = e.target.files[0];
    
        reader.onloadend = () => {
          this.setState({
            file: file,
            imagePreviewUrl: reader.result
          });
        }
    
        reader.readAsDataURL(file)
      }
    
    handleChange = event =>{
        this.setState({TITLE: event.target.value});
    }
    preview_image(event) 
    {
     var reader = new FileReader();
    reader.onload = function()
     {
      var output = document.getElementById('output_image');
      output.src = reader.result;
     }
     console.log(event.target.files[0]);
     
     reader.readAsDataURL(event.target.files[0]);
    }

    handleAdd(){
        document.getElementById("text1").style.display = "none";
        document.getElementById("text2").style.display = "none";
        var titlee= document.getElementById("title").value;
        var desc=document.getElementById("des").value;
        if(titlee==""&&desc==""){
            document.getElementById("text1").style.display = "inline";
            document.getElementById("text2").style.display = "inline";
          return;
        }
        if(titlee==""){
            document.getElementById("text1").style.display = "inline";
          return;
        }
        if(desc==""){
            document.getElementById("text2").style.display = "inline";
          return;
        }

            Axios.post('http://110.74.194.124:15011/v1/api/articles', 
            {
              "TITLE": titlee,
              "DESCRIPTION": desc,
              "AUTHOR": 0,
              "CATEGORY_ID": 0,
              "STATUS": "single",
              "IMAGE": this.state.imagePreviewUrl
            }
        ).then(res=>{
          console.log(res.data);
          document.getElementById("text1").style.display = "none";
          document.getElementById("text2").style.display = "none";
          document.getElementById("title").value="";
          document.getElementById("des").value="";
            }
          );
          console.log("This is: "+titlee);
          
            alert("Yayyy! Successfully Added.")
    
    }

    formPreventDefault= event => {
        event.preventDefault();
    }
    
    render() {
     
    
        let styleA={background:"#e0f2f1"}
        let styleB={textAlign:"center"}
        let styleC={display:"flex"}
        let styleD={color:"red", display:"none"}
        let styleE={width:"600px"}
        let styleF={color:"red", display:"none"}
        let styleG={marginRight:"50px"}
        let styleH={marginLeft:"50px"}
        let styleI={marginTop:"30px"}
        let styleJ={marginTop:"30px",marginLeft: "250px",width: "150px"}

        return (
            <div style={styleA}>
            <h3 style={styleB}>Add an Article</h3>
            <Container style={{styleC}}>
                <Row style={styleG}>
                  <col-9>
                    <div >
                      <div>
                        TITLE <span id="text1" style={styleD}> Sorry! The input cannot be NULL or EMPTY.</span>
                      </div>
                      <input id="title" style={styleE} type="text" placeholder="Enter Title"/>
                      <div style={styleI}>Description <span id="text2" style={styleF}> Sorry! The input cannot be NULL or EMPTY.</span></div>
                      <input type="text" id="des"  style={styleE} placeholder="Enter Description"  />
                    </div>
                    <Button variant="info" style={styleJ} onClick={this.handleAdd} type="submit">
                            Add
                    </Button>
                  </col-9>
                  <col-9 style={styleH}>
                    <div >
                      <img width="155px" src={this.state.imagePreviewUrl} />
                          <input className="fileInput" type="file"  onChange={(e)=>this._handleImageChange(e)} />
                    </div>
                  </col-9>
                </Row>
            </Container>
            </div>
        )
    }
}
