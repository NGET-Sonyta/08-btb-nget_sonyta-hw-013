import React, { Component } from 'react';
import Axios from 'axios';
import List from './Components/List';
import NavBarr from './Components/NavBarr'
import ArticleInput from './Components/ArticleInput';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import CardCompo from './Components/CardCompo';
import UpdateCompo from './Components/UpdateCompo';
class App extends Component {
  constructor(){
    super();

    this.state={
      loading:true,
    }
  }

componentDidMount(){

}

  render() {
    // if(this.state.loading){
    //     return <h1>Loading</h1>;
    // }
    return (
      <div>
        <Router>
          <NavBarr/>
          <Switch>
        <Route path='/ArticleInput' component={ArticleInput} />
        <Route path='/View/id=:id' component={CardCompo} />
        <Route path='/Update/id=:id' component={UpdateCompo} />
        <Route path='/Home' exact component={List} />
        <Route path='*' component={List} />
        {/* <List/> */}
        </Switch>
        </Router>
      </div>

    )
  }
}

export default App;

